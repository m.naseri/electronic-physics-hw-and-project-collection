import matplotlib.pyplot as plt
import numpy as np
import math


m0 = 9.10938356e-31  # Kg
mn = 1.1 * m0
mp = 0.59 * m0
T = 300  # K
h = 6.626070040e-34  # Js
hBar = h / (2 * math.pi)
k = 1.38064852e-23  # J⋅K−1
Ec = 1.12*1.6021766208e-19/2  # J
Ev = -Ec
EF = 0

EUp = np.arange(Ec, 1.25*Ec, Ec/1000)
gc = mn*np.sqrt(2*mn*(EUp - Ec)) / (math.pi ** 2 * (hBar ** 3))
EDown = np.arange(1.25*Ev, Ev, -Ev/1000)
gv = mp*np.sqrt(2*mp*(Ev - EDown)) / (math.pi ** 2 * (hBar ** 3))
# E = np.arange(EDown[0], EUp[-1], Ec/100)
# f = 1/(1 + np.exp((E-EF)/(k*T)))
fUp = 1/(1 + np.exp((EUp-EF)/(k*T)))
fDown = 1/(1 + np.exp((EDown-EF)/(k*T)))
functionUp = fUp * gc
functionDown = (1-fDown) * gv

fig, ax = plt.subplots()
# ax.plot(gv, EDown)
# ax.plot(gc, EUp)
# ax.plot(E, f)
ax.plot(functionDown, EDown)
ax.plot(functionUp, EUp)
# plt.yscale('log')

ax.set(xlabel='', ylabel='',
       title='ElectronicPhysicsHW')
ax.grid()
fig.savefig("test.png")
plt.show()
fun