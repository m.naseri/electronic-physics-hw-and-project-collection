import numpy as np
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import math

m0 = 9.10938356e-31
mn = 1.1 * m0
mp = 0.59 * m0
T = 300
h = 6.62607004e-34
hBar = h / (2 * math.pi)
Ei = 1.12 * 1.6021766208e-19 / 2
Ec = 1.12 * 1.6021766208e-19
Ev = 0
k = 1.38064852e-23
Ef = np.arange(Ev + 3 * k * T, Ec - 8 * k * T, (Ec - 11 * k * T) / 1000)
etaF = (Ef - Ec) / (k * T)
constant = (mn * np.sqrt(2 * mn) * ((k * T) ** 1.5)) / ((math.pi ** 2) * (hBar ** 3))
Nc = (np.sqrt(math.pi)/2) * constant

X = 0
n1 = []


def integrand(eta):
    return np.sqrt(eta) / (1 + np.exp(eta - X))


for i in etaF:
    X = i
    n1.append(integrate.quad(integrand, 0, np.inf)[0] * constant)


n2 = Nc * np.exp((Ef - Ec) / (k * T))

result = []
for j in range(0, 1000):
    result = (n1[j] - n2[j]) / n1[j] * 100


# x = np.array(Ef)
# y = np.array(result)

fig, ax = plt.subplots()
ax.plot(result, Ef)
fig.savefig("HW4.png")
plt.show()




