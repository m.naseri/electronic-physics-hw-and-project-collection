import matplotlib.pyplot as plt
import numpy as np
import math

m0 = 9.10938356e-31  # Kg
mn = 1.18 * m0
mp = 0.81 * m0
h = 6.626070040e-34  # Js
hBar = h / (2 * math.pi)
k = 1.38064852e-23  # J⋅K−1
Ec = 1.12 * 1.6021766208e-19 / 2  # J
Ev = -Ec
q = 1.6e-19

T = np.arange(100, 650, (650 - 100) / 10000)
T = [300]
Nc = [((k * T[i]) ** 1.5) * (mn * math.sqrt(2 * mn) /
                             (math.pi ** 2 * hBar ** 3)) *
      math.sqrt(math.pi) / 2 for i in range(len(T))]
Nv = [((k * T[i]) ** 1.5) * (mp * math.sqrt(2 * mp) /
                             (math.pi ** 2 * hBar ** 3)) *
      math.sqrt(math.pi) / 2 for i in range(len(T))]

ni = [(math.sqrt(Nc[i] * Nv[i]) * math.exp((Ev - Ec) / (2 * k * T[i])))
      / 1e6 for i in range(len(T))]

ND = 1.13e18  # 10^15 / cm^3
NA = ND  # 10^15 / cm^3
ni = [1e10]

Vbi = [k * T[i] / q * np.log(NA * ND / (ni[i] ** 2)) for i in range(len(T))]
print(Vbi)
W = [np.sqrt(2 * 11.8 * 8.55e-14 / q * Vbi[i] * (NA + ND) / (NA * ND))
     for i in range(len(T))]  # cm
print(W)

fig, ax = plt.subplots()
ax.plot(T, W)
ax.grid()
fig.savefig("sh.png")
plt.show()
