import matplotlib.pyplot as plt
import numpy as np
import math

m0 = 9.10938356e-31
mn = 1.18 * m0
mp = 0.81 * m0
T = 300
h = 6.62607004e-34
hBar = h / (2 * math.pi)
k = 1.38064852e-23
Ei = 0

P0 = 1e5  # /cm^3
Lp = 3.44e-3  # cm
deltaPn0 = 1e10  # /cm^3
ni = 1e10

x = np.arange(0, 7 * Lp, Lp / 100)
deltaPn = deltaPn0 * np.exp(-x / Lp)
P = P0 + deltaPn
Fp = Ei - k * T * np.log(P / ni)  # J

fig, ax = plt.subplots()
ax.plot(x / Lp, deltaPn)
# ax.plot(x / Lp, Fp)

ax.grid()
fig.savefig("deltaPn.png")
plt.show()
