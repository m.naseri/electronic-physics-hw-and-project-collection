import numpy as np
import scipy.integrate as integrate
import math

m0 = 9.10938356e-31
mn = 1.18 * m0
mp = 0.81 * m0
T = 300
h = 6.62607004e-34
hBar = h / (2 * math.pi)

Ec = 1.12 * 1.6021766208e-19
Ev = 0
k = 1.38064852e-23
Nc = ((k * T) ** 1.5) * (mn * math.sqrt(2 * mn) /
                         (math.pi ** 2 * hBar ** 3)) * math.sqrt(math.pi) / 2
Nv = ((k * T) ** 1.5) * (mp * math.sqrt(2 * mp) /
                         (math.pi ** 2 * hBar ** 3)) * math.sqrt(math.pi) / 2
Ei = (Ec + Ev) / 2 + k * T * math.log(Nv / Nc) / 2
EF = Ei


def nCalculator(E):
    gc = mn * np.sqrt(2 * mn * (E - Ec)) / (math.pi ** 2 * (hBar ** 3))
    return gc / (1 + np.exp((E - EF) / (k * T)))


def expectedValue(E):
    gc = mn * np.sqrt(2 * mn * (E - Ec)) / (math.pi ** 2 * (hBar ** 3))
    f = gc / (1 + np.exp((E - EF) / (k * T)))
    return f / ni * E


ni = integrate.quad(nCalculator, Ec, Ec * 16)[0]
result = integrate.quad(expectedValue, Ec, 16 * Ec)[0] / 1.6021766208e-19
print(result)  # 1.2306632481947 eV
